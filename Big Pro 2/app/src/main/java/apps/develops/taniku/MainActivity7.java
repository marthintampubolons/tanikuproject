package apps.develops.taniku;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import apps.develops.taniku.api.Api6;
import apps.develops.taniku.api.Retroserver5;
import apps.develops.taniku.model.ResponsModel5;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity7 extends AppCompatActivity {
    EditText nama,jenis,harga;
    Button btnsave, btnTampildata, btnupdate,btndelete;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);


        nama = (EditText) findViewById(R.id.edt_nama);
        jenis = (EditText) findViewById(R.id.edt_jenis);
        harga = (EditText) findViewById(R.id.edt_harga);

        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);

        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if(iddata != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            nama.setText(data.getStringExtra("nama"));
            jenis.setText(data.getStringExtra("jenis"));
            harga.setText(data.getStringExtra("harga"));

        }

        pd = new ProgressDialog(this);



        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(MainActivity7.this, apps.develops.taniku.TampilData5.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                Api6 api = Retroserver5.getClient().create(Api6.class);
                Call<ResponsModel5> del  = api.deleteData(iddata);
                del.enqueue(new Callback<ResponsModel5>() {
                    @Override
                    public void onResponse(Call<ResponsModel5> call, Response<ResponsModel5> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity7.this, response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new Intent(MainActivity7.this,TampilData5.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel5> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("update ....");
                pd.setCancelable(false);
                pd.show();

                Api6 api = Retroserver5.getClient().create(Api6.class);
                Call<ResponsModel5> update = api.updateData(iddata,nama.getText().toString(),jenis.getText().toString(),harga.getText().toString());
                update.enqueue(new Callback<ResponsModel5>() {
                    @Override
                    public void onResponse(Call<ResponsModel5> call, Response<ResponsModel5> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(MainActivity7.this,response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<ResponsModel5> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "OnFailure");

                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ... ");
                pd.setCancelable(false);
                pd.show();

                String snama = nama.getText().toString();
                String sjenis = jenis.getText().toString();
                String sharga = harga.getText().toString();
                Api6 api = Retroserver5.getClient().create(Api6.class);

                Call<ResponsModel5> sendbio = api.sendBiodata(snama,sjenis,sharga);
                sendbio.enqueue(new Callback<ResponsModel5>() {
                    @Override
                    public void onResponse(Call<ResponsModel5> call, Response<ResponsModel5> response) {
                        pd.hide();
                        Log.d("RETRO", "response : " + response.body().toString());
                        String kode = response.body().getKode();

                        if(kode.equals("1"))
                        {
                            Toast.makeText(MainActivity7.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(MainActivity7.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel5> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}

