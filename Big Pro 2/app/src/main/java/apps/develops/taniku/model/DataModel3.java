package apps.develops.taniku.model;


/**
 * Created by hakiki95 on 4/16/2017.
 */

public class DataModel3 {
    String id, nama, jenis_komoditas,pendapatan;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis_komoditas() {
        return jenis_komoditas;
    }

    public void setJenis_komoditas(String jenis_komoditas) {
        this.jenis_komoditas = jenis_komoditas;
    }

    public String getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(String pendapatan) {
        this.pendapatan = pendapatan;
    }



}

