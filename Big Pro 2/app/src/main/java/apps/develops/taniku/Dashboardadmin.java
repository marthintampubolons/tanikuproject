package apps.develops.taniku;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class Dashboardadmin extends AppCompatActivity {
    ImageView gbr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboardadmin);

        gbr = findViewById(R.id.gambarr);

        Button btnyeActivity = findViewById(R.id.btn_ye_activity);

        btnyeActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboardadmin.this, MainActivity6.class);
                startActivity(intent);
            }
        });

        Button lahan = findViewById(R.id.lahan);

        lahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboardadmin.this, TampilData3.class);
                startActivity(intent);
            }
        });



        Button komoditas = findViewById(R.id.komoditas);

        komoditas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboardadmin.this, TampilData2.class);
                startActivity(intent);
            }
        });

        Button tes = findViewById(R.id.tes);

        tes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboardadmin.this, TampilData.class);
                startActivity(intent);
            }
        });

        Button pupuk = findViewById(R.id.pupuk);

        pupuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboardadmin.this, MainActivity7.class);
                startActivity(intent);
            }
        });

        Button foto = findViewById(R.id.foto);

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboardadmin.this, FoodList.class);
                startActivity(intent);
            }
        });



      //  gbr.setOnClickListener(new View.OnClickListener() {
           // @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
//                startActivity(intent);
//            }
      //  });
    }
}

