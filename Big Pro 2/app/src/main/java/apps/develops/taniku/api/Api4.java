package apps.develops.taniku.api;



import apps.develops.taniku.model.ResponsModel3;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public interface Api4 {

    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponsModel3> sendBiodata(@Field("nama") String nama,
                                    @Field("jenis_komoditas") String jenis_komoditas,
                                    @Field("pendapatan") String pendapatan);

    @GET("read.php")
    Call<ResponsModel3> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel3> updateData( @Field("id") String id,
                                    @Field("nama") String nama,
                                    @Field("jenis_komoditas") String jenis_komoditas,

                                    @Field("pendapatan") String pendapatan);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel3> deleteData(@Field("id") String id);
}
