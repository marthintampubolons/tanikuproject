package apps.develops.taniku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import apps.develops.taniku.api.LoginResponses;
import apps.develops.taniku.api.RetrofitClients;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login2 extends AppCompatActivity  implements View.OnClickListener {
    private EditText editEmail,editPassword;
    private TextView register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);

        findViewById(R.id.butLogin).setOnClickListener(this);

//        register = findViewById(R.id.register);
//        register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mainIntent = new Intent(getApplicationContext(), Register2.class);
//                startActivity(mainIntent);
//            }
//        });
    }

    private void userLogin(){
        String email = editEmail.getText().toString().trim();
        String password = editPassword.getText().toString().trim();

        if(email.isEmpty()){
            editEmail.setError("Email is required");
            editEmail.requestFocus();
            return;
        }if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editEmail.setError("Enter a valid Email");
            editEmail.requestFocus();
            return;
        }if(password.isEmpty()){
            editPassword.setError("Password is required");
            editPassword.requestFocus();
            return;
        }if(password.length() < 6){
            editPassword.setError("Password should be 6 character    ");
            editPassword.requestFocus();
            return;
        }

        Call<LoginResponses> call = RetrofitClients
                .getInstance()
                .getApi()
                .userLogin(email,password);

        call.enqueue(new Callback<LoginResponses>() {
            @Override
            public void onResponse(Call<LoginResponses> call, Response<LoginResponses> response) {
                LoginResponses loginResponse = response.body();
                if(!loginResponse.isError()){
                    Toast.makeText( Login2.this,loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                    Intent mainIntent = new Intent(getApplicationContext(),Dashboardadmin.class);
                    startActivity(mainIntent);
                }else{
                    Toast.makeText( Login2.this,"Email dan Password anda salah",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<LoginResponses> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }




    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.butLogin:
                userLogin();
                break;
        }
    }

}

