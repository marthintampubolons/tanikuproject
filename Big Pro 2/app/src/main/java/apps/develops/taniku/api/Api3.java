package apps.develops.taniku.api;



import apps.develops.taniku.model.ResponsModel2;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public interface Api3 {

    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponsModel2> sendBiodata(@Field("nama") String nama,
                                   @Field("waktu") String waktu,
                                   @Field("jenis") String jenis,
                                    @Field("alamat") String alamat);

    @GET("read.php")
    Call<ResponsModel2> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel2> updateData( @Field("id") String id,
                                   @Field("nama") String nama,
                                   @Field("waktu") String waktu,
                                   @Field("jenis") String jenis,
                                   @Field("alamat") String alamat);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel2> deleteData(@Field("id") String id);
}
