
package apps.develops.taniku.model;



import java.util.List;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public class ResponsModel5 {

    String  kode, pesan;
    List<DataModel5> result;

    public List<DataModel5> getResult() {
        return result;
    }

    public void setResult(List<DataModel5> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}

