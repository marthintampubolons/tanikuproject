package apps.develops.taniku;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import apps.develops.taniku.api.Api4;
import apps.develops.taniku.api.Retroserver3;
import apps.develops.taniku.model.ResponsModel3;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity5 extends AppCompatActivity {
    EditText nama, jenis_komoditas,pendapatan;
    Button btnsave, btnTampildata, btnupdate,btndelete;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);


        nama = (EditText) findViewById(R.id.edt_nama);
        jenis_komoditas = (EditText) findViewById(R.id.edt_jenis_komoditas);
        pendapatan = (EditText) findViewById(R.id.edtpendapatan);

        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);

        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if(iddata != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            nama.setText(data.getStringExtra("nama"));
            jenis_komoditas.setText(data.getStringExtra("jenis_komoditas"));
            pendapatan.setText(data.getStringExtra("pendapatan"));

        }

        pd = new ProgressDialog(this);



        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(MainActivity5.this, apps.develops.taniku.TampilData3.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                Api4 api = Retroserver3.getClient().create(Api4.class);
                Call<ResponsModel3> del  = api.deleteData(iddata);
                del.enqueue(new Callback<ResponsModel3>() {
                    @Override
                    public void onResponse(Call<ResponsModel3> call, Response<ResponsModel3> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity5.this, response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new Intent(MainActivity5.this,TampilData3.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel3> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("update ....");
                pd.setCancelable(false);
                pd.show();

                Api4 api = Retroserver3.getClient().create(Api4.class);
                Call<ResponsModel3> update = api.updateData(iddata,nama.getText().toString(),jenis_komoditas.getText().toString(),pendapatan.getText().toString());
                update.enqueue(new Callback<ResponsModel3>() {
                    @Override
                    public void onResponse(Call<ResponsModel3> call, Response<ResponsModel3> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(MainActivity5.this,response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<ResponsModel3> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "OnFailure");

                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ... ");
                pd.setCancelable(false);
                pd.show();

                String snama = nama.getText().toString();
                String sjenis_komoditas = jenis_komoditas.getText().toString();
                String spendapatan = pendapatan.getText().toString();

                Api4 api = Retroserver3.getClient().create(Api4.class);

                Call<ResponsModel3> sendbio = api.sendBiodata(snama,sjenis_komoditas,spendapatan);
                sendbio.enqueue(new Callback<ResponsModel3>() {
                    @Override
                    public void onResponse(Call<ResponsModel3> call, Response<ResponsModel3> response) {
                        pd.hide();
                        Log.d("RETRO", "response : " + response.body().toString());
                        String kode = response.body().getKode();

                        if(kode.equals("1"))
                        {
                            Toast.makeText(MainActivity5.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(MainActivity5.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel3> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}

