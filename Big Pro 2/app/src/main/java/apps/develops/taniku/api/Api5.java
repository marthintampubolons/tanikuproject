package apps.develops.taniku.api;



import apps.develops.taniku.model.ResponsModel4;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public interface Api5 {

    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponsModel4> sendBiodata(@Field("judul") String judul,
                                    @Field("deskripsi") String deskripsi);

    @GET("read.php")
    Call<ResponsModel4> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel4> updateData( @Field("id") String id,
                                    @Field("judul") String judul,
                                    @Field("deskripsi") String deskripsi);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel4> deleteData(@Field("id") String id);
}
