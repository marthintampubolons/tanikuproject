package apps.develops.taniku.api;



import apps.develops.taniku.model.ResponsModel5;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public interface Api6 {

    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponsModel5> sendBiodata(@Field("nama") String nama,
                                    @Field("jenis") String jenis,
                                    @Field("harga") String harga);

    @GET("read.php")
    Call<ResponsModel5> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel5> updateData( @Field("id") String id,
                                    @Field("nama") String nama,
                                    @Field("jenis") String jenis,
                                    @Field("harga") String harga);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel5> deleteData(@Field("id") String id);
}
