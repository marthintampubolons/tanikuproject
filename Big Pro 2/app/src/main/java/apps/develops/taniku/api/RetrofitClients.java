package apps.develops.taniku.api;





import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClients {

    private static final String BASE_URL = "http://192.168.43.5/API_login/public/";
    private static RetrofitClients mInstance;
    private Retrofit retrofit;

    private RetrofitClients() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClients getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClients();
        }
        return mInstance;
    }

    public Apis getApi() {
        return retrofit.create( Apis.class);
    }
}
