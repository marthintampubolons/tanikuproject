package apps.develops.taniku.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import apps.develops.taniku.MainActivity4;
import apps.develops.taniku.R;
import apps.develops.taniku.model.DataModel2;

import java.util.List;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public class AdapterData2 extends RecyclerView.Adapter<AdapterData2.HolderData> {

    private List<DataModel2> mList ;
    private Context ctx;


    public  AdapterData2 (Context ctx, List<DataModel2> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist2,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel2 dm = mList.get(position);
        holder.nama.setText(dm.getNama());
        holder.waktu.setText(dm.getWaktu());
        holder.jenis.setText(dm.getJenis());
        holder.alamat.setText(dm.getAlamat());
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView nama, waktu, jenis,alamat;
        DataModel2 dm;
        public HolderData (View v)
        {
            super(v);

            nama  = (TextView) v.findViewById(R.id.tvNama);
            waktu = (TextView) v.findViewById(R.id.tvWaktu);
            jenis = (TextView) v.findViewById(R.id.tvJenis);
            alamat = (TextView) v.findViewById(R.id.tvAlamat);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, MainActivity4.class);
                    goInput.putExtra("id", dm.getId());
                    goInput.putExtra("nama", dm.getNama());
                    goInput.putExtra("waktu", dm.getWaktu());
                    goInput.putExtra("jenis", dm.getJenis());
                    goInput.putExtra("alamat", dm.getAlamat());

                    ctx.startActivity(goInput);
                }
            });
        }
    }
}
