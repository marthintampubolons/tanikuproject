
package apps.develops.taniku.model;



import java.util.List;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public class ResponsModel3 {

    String  kode, pesan;
    List<DataModel3> result;

    public List<DataModel3> getResult() {
        return result;
    }

    public void setResult(List<DataModel3> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}

