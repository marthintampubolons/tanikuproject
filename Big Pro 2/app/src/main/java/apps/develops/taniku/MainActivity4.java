package apps.develops.taniku;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import apps.develops.taniku.api.Api3;
import apps.develops.taniku.api.Retroserver2;
import apps.develops.taniku.model.ResponsModel2;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity4 extends AppCompatActivity {
    EditText nama, waktu, jenis,alamat;
    Button btnsave, btnTampildata, btnupdate,btndelete;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);


        nama = (EditText) findViewById(R.id.edt_nama);
        waktu = (EditText) findViewById(R.id.edt_waktu);
        jenis = (EditText) findViewById(R.id.edtjenis);
        alamat = (EditText) findViewById(R.id.edtalamat);
        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);

        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if(iddata != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            nama.setText(data.getStringExtra("nama"));
            waktu.setText(data.getStringExtra("waktu"));
            jenis.setText(data.getStringExtra("jenis"));
            alamat.setText(data.getStringExtra("alamat"));
        }

        pd = new ProgressDialog(this);



        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(MainActivity4.this, apps.develops.taniku.TampilData2.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                Api3 api = Retroserver2.getClient().create(Api3.class);
                Call<ResponsModel2> del  = api.deleteData(iddata);
                del.enqueue(new Callback<ResponsModel2>() {
                    @Override
                    public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity4.this, response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new Intent(MainActivity4.this,TampilData2.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel2> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("update ....");
                pd.setCancelable(false);
                pd.show();

                Api3 api = Retroserver2.getClient().create(Api3.class);
                Call<ResponsModel2> update = api.updateData(iddata,nama.getText().toString(),waktu.getText().toString(),jenis.getText().toString(),alamat.getText().toString());
                update.enqueue(new Callback<ResponsModel2>() {
                    @Override
                    public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(MainActivity4.this,response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<ResponsModel2> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "OnFailure");

                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ... ");
                pd.setCancelable(false);
                pd.show();

                String snama = nama.getText().toString();
                String swaktu = waktu.getText().toString();
                String sjenis = jenis.getText().toString();
                String salamat = alamat.getText().toString();
                Api3 api = Retroserver2.getClient().create(Api3.class);

                Call<ResponsModel2> sendbio = api.sendBiodata(snama,swaktu,sjenis,salamat);
                sendbio.enqueue(new Callback<ResponsModel2>() {
                    @Override
                    public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                        pd.hide();
                        Log.d("RETRO", "response : " + response.body().toString());
                        String kode = response.body().getKode();

                        if(kode.equals("1"))
                        {
                            Toast.makeText(MainActivity4.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(MainActivity4.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel2> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}

