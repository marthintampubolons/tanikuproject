package apps.develops.taniku;



import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class Splashscreen extends AppCompatActivity {
    private ProgressBar progressBar = null;
    private AsyncTaskRunner asyncTaskRunner = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splashscreen );
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getProgressDrawable().setColorFilter( Color.RED, PorterDuff.Mode.SRC_IN );

        asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();
        scale();
        new Handler().postDelayed(
                new Runnable() {

                    public void run() {

                        //perpindahan splashcreen menuju tampilan home
                        Intent i = new Intent( getApplicationContext(),
                                Pertama.class );
                        startActivity( i );
                        finish();
                    }
                }, 3000 ); //delay 3 detik
    }


    public void scale() {
        ImageView image = (ImageView) findViewById( R.id.imageView );
        //set animasi yang akan dilakukan
        Animation animation = AnimationUtils.loadAnimation( getApplicationContext(), R.anim.scale );
        //start animasi
        image.startAnimation( animation );
    }

    class AsyncTaskRunner extends AsyncTask<Void, Integer, Integer> {
        private int progress = 0;

        @Override
        protected Integer doInBackground(Void... voids) {
            for (int i = 0; i <= 20; i++) {
                publishProgress( progress );
                final int value = i;
                try {
                    Thread.sleep( 200 );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                progress = value;
            }
            return progress;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress( values[0] );
        }

    }
}

