package apps.develops.taniku.api;





import apps.develops.taniku.User2;

public class LoginResponses {
    private boolean error;
    private String message;
    private User2 user;

    public LoginResponses(boolean error, String message, User2 user) {
        this.error = error;
        this.message = message;
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public User2 getUser() {
        return user;
    }
}
