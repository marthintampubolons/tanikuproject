package apps.develops.taniku.adapter;




import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import apps.develops.taniku.MainActivity6;
import apps.develops.taniku.R;
import apps.develops.taniku.model.DataModel4;

import java.util.List;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public class AdapterData4 extends RecyclerView.Adapter<AdapterData4.HolderData> {

    private List<DataModel4> mList ;
    private Context ctx;


    public  AdapterData4 (Context ctx, List<DataModel4> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist4,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel4 dm = mList.get(position);
        holder.judul.setText(dm.getJudul());
        holder.deskripsi.setText(dm.getDeskripsi());


        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView judul,deskripsi;
        DataModel4 dm;
        public HolderData (View v)
        {
            super(v);

            judul  = (TextView) v.findViewById(R.id.tvJudul);
             deskripsi = (TextView) v.findViewById(R.id.tvDeskripsi);


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, MainActivity6.class);
                    goInput.putExtra("id", dm.getId());
                    goInput.putExtra("judul", dm.getJudul());
                    goInput.putExtra("deskripsi", dm.getDeskripsi());


                    ctx.startActivity(goInput);
                }
            });
        }
    }
}
