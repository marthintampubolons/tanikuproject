package apps.develops.taniku.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import apps.develops.taniku.MainActivity5;
import apps.develops.taniku.R;
import apps.develops.taniku.model.DataModel3;

import java.util.List;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public class AdapterData3 extends RecyclerView.Adapter<AdapterData3.HolderData> {

    private List<DataModel3> mList ;
    private Context ctx;


    public  AdapterData3 (Context ctx, List<DataModel3> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist3,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel3 dm = mList.get(position);
        holder.nama.setText(dm.getNama());
        holder.jenis_komoditas.setText(dm.getJenis_komoditas());
        holder.pendapatan.setText(dm.getPendapatan());

        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView nama, jenis_komoditas, pendapatan;
        DataModel3 dm;
        public HolderData (View v)
        {
            super(v);

            nama  = (TextView) v.findViewById(R.id.tvNama);
            jenis_komoditas = (TextView) v.findViewById(R.id.tvJenis_komoditas);
            pendapatan = (TextView) v.findViewById(R.id.tvPendapatan);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, MainActivity5.class);
                    goInput.putExtra("id", dm.getId());
                    goInput.putExtra("nama", dm.getNama());
                    goInput.putExtra("jenis_komoditas", dm.getJenis_komoditas());
                    goInput.putExtra("pendapatan", dm.getPendapatan());


                    ctx.startActivity(goInput);
                }
            });
        }
    }
}
