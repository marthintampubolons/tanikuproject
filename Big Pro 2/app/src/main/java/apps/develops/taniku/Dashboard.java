package apps.develops.taniku;


import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ViewFlipper;
import android.view.View;
import android.widget.Button;

public class Dashboard extends AppCompatActivity {

    ViewFlipper v_flipper;

    public Dashboard() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        int image[] = {R.drawable.petani1,R.drawable.petani2,R.drawable.petani3,R.drawable.petani4,R.drawable.petani5};
        v_flipper = findViewById(R.id.v_flipper);

        for(int i=0;i<image.length;i++){
            fliveImages(image[i]);
        }
        for(int images : image)
            fliveImages(images);

        Button btnSecondActivity = findViewById(R.id.btn_second_activity);

        btnSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, MainActivity3.class);
                startActivity(intent);
            }
        });
        Button btnThirdActivity = findViewById(R.id.btn_third_activity);

        btnThirdActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, MainActivity4.class);
                startActivity(intent);
            }
        });
        Button btnFourActivity = findViewById(R.id.btn_four_activity);

        btnFourActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, MainActivity5.class);
                startActivity(intent);
            }
        });

        Button btnyoActivity = findViewById(R.id.btn_yo_activity);

        btnyoActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, gambar.class);
                startActivity(intent);
            }
        });

        Button btnyiActivity = findViewById(R.id.btn_yi_activity);

        btnyiActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, TampilData4.class);
                startActivity(intent);
            }
        });

        Button pupuks = findViewById(R.id.pupuks);

        pupuks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, TampilData5.class);
                startActivity(intent);
            }
        });
    }



    public void fliveImages(int images){
        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setBackgroundResource(images);

        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(5000);
        v_flipper.setAutoStart(true);


//      v_flipper.setInAnimation(getActivity(),R.anim.blinkfrag);
        v_flipper.setInAnimation(getApplicationContext(),android.R.anim.slide_in_left);
//      v_flipper.setInAnimation(getContext(),android.R.anim.slide_out_right);


    }
}
