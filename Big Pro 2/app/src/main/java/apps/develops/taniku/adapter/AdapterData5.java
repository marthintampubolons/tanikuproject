package apps.develops.taniku.adapter;




import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import apps.develops.taniku.MainActivity7;
import apps.develops.taniku.R;
import apps.develops.taniku.model.DataModel5;

import java.util.List;

/**
 * Created by hakiki95 on 4/16/2017.
 */

public class AdapterData5 extends RecyclerView.Adapter<AdapterData5.HolderData> {

    private List<DataModel5> mList ;
    private Context ctx;


    public  AdapterData5 (Context ctx, List<DataModel5> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist5,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel5 dm = mList.get(position);
        holder.nama.setText(dm.getNama());
        holder.jenis.setText(dm.getJenis());
        holder.harga.setText(dm.getHarga());

        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView nama,jenis,harga;
        DataModel5 dm;
        public HolderData (View v)
        {
            super(v);

            nama  = (TextView) v.findViewById(R.id.tvNama);
            jenis = (TextView) v.findViewById(R.id.tvJenis);
            harga = (TextView) v.findViewById(R.id.tvHarga);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, MainActivity7.class);
                    goInput.putExtra("id", dm.getId());
                    goInput.putExtra("nama", dm.getNama());
                    goInput.putExtra("jenis", dm.getJenis());
                    goInput.putExtra("harga", dm.getHarga());

                    ctx.startActivity(goInput);
                }
            });
        }
    }
}
