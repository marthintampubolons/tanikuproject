package apps.develops.taniku;



public class User2 {
    private int id;
    private String email,password,nama;

    public User2(int id, String email, String password, String nama) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getNama() {
        return nama;
    }
}
