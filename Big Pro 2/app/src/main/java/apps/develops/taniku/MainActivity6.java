package apps.develops.taniku;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import apps.develops.taniku.api.Api5;
import apps.develops.taniku.api.Retroserver4;
import apps.develops.taniku.model.ResponsModel4;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity6 extends AppCompatActivity {
    EditText judul,deskripsi;
    Button btnsave, btnTampildata, btnupdate,btndelete;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);


        judul = (EditText) findViewById(R.id.edt_judul);
        deskripsi = (EditText) findViewById(R.id.edt_deskripsi);


        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);

        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if(iddata != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            judul.setText(data.getStringExtra("judul"));
            deskripsi.setText(data.getStringExtra("deskripsi"));


        }

        pd = new ProgressDialog(this);



        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(MainActivity6.this, apps.develops.taniku.TampilData4.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                Api5 api = Retroserver4.getClient().create(Api5.class);
                Call<ResponsModel4> del  = api.deleteData(iddata);
                del.enqueue(new Callback<ResponsModel4>() {
                    @Override
                    public void onResponse(Call<ResponsModel4> call, Response<ResponsModel4> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity6.this, response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new Intent(MainActivity6.this,TampilData4.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel4> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("update ....");
                pd.setCancelable(false);
                pd.show();

                Api5 api = Retroserver4.getClient().create(Api5.class);
                Call<ResponsModel4> update = api.updateData(iddata,judul.getText().toString(),deskripsi.getText().toString());
                update.enqueue(new Callback<ResponsModel4>() {
                    @Override
                    public void onResponse(Call<ResponsModel4> call, Response<ResponsModel4> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(MainActivity6.this,response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<ResponsModel4> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "OnFailure");

                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ... ");
                pd.setCancelable(false);
                pd.show();

                String sjudul = judul.getText().toString();
                String sdeskripsi = deskripsi.getText().toString();

                Api5 api = Retroserver4.getClient().create(Api5.class);

                Call<ResponsModel4> sendbio = api.sendBiodata(sjudul,sdeskripsi);
                sendbio.enqueue(new Callback<ResponsModel4>() {
                    @Override
                    public void onResponse(Call<ResponsModel4> call, Response<ResponsModel4> response) {
                        pd.hide();
                        Log.d("RETRO", "response : " + response.body().toString());
                        String kode = response.body().getKode();

                        if(kode.equals("1"))
                        {
                            Toast.makeText(MainActivity6.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(MainActivity6.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel4> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}

