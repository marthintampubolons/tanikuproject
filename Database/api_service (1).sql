-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Bulan Mei 2019 pada 16.49
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_service`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `nama`) VALUES
(21, 'jn@gmail.com', '$2y$10$PQjlR2ynj1kRSiWTTrzBle.47EjI8NLKKVniC6WhveJYUysqpmsWO', 'hahahahaha'),
(26, 'marthin@gmail.com', '$2y$10$CRR.62HfGTvXrJNwM9lg7.nFHqu6..j4qW1Hj1kZHShCOoUtG6ii.', 'marthin'),
(27, 'marthin2@gmail.com', '$2y$10$EM.B0xTgMHTA4vGEsLsAyeGv8NysfTvElSQjF4iRmeELweAelflwW', 'Marthin'),
(28, 'marthin3@gmail.com', '$2y$10$trmfTvTkFRkBln8QK17qnuGx8UUS//Z1I/LvjjHey3LI73AfIIE3S', 'Marthin'),
(29, 'jh61@gmail.com', '$2y$10$5JrRqnCa.QFrjIdXcXxHr.B/7KJGLVh4SiOx9Mnsk1hC1f.YYBtyC', 'jkk'),
(30, 'marthin36@gmail.com', '$2y$10$F/RVmmVxdUm4/seLpLjTt.Z8Hx8TCpTYIpSjitXej44ro1D66eUrW', 'marthin'),
(31, 'a@gmail.com', '$2y$10$DjVZNfi91i2PyC0hwTzcVeLjukMJ50PFyLhC8ltKdwV7KwUPuRP4K', 'aaaaaa'),
(32, 'palti@gmail.com', '$2y$10$cUNScnuohJhxy59rbrJSoOhx/.wJQzf2g1MsI.J1znXBOMXAMOewW', 'palti');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
