<?php

  class DBOperations{

    private $con;

    function __construct(){
      require_once dirname(__FILE__) . '/DBConnect.php';
      $db = new DBConnect();
      $this->con = $db->connect();
    }

    public function createUser($email,$password,$nama){
      if(!$this->isEmailExist($email)){
          $stmt = $this->con->prepare("INSERT INTO users(email,password,nama) VALUES(?,?,?)");
          $stmt->bind_param("sss",$email,$password,$nama);
          if($stmt->execute()){
            return USER_CREATED;
          }else{
            return USER_FAILURE;  
          }
          return USER_EXISTS;  
      }
    }

     public function getAllUsers(){
            $stmt = $this->con->prepare("SELECT id, email, password, nama FROM users;");
            $stmt->execute(); 
            $stmt->bind_result($id, $email, $password, $nama);
            $users = array(); 
            while($stmt->fetch()){ 
                $user = array(); 
                $user['id'] = $id; 
                $user['email']=$email; 
                $user['password'] = $password; 
                $user['nama'] = $nama; 
                array_push($users, $user);
            }             
            return $users; 
        }

    public function updateUser($email,$password,$nama,$id){
      $stmt = $this->con->prepare("UPDATE users SET email = ?,password = ?,nama = ? WHERE id = ?");
      $stmt->bind_param("sssi",$email,$password,$nama,$id);
      if($stmt->execute()){
        return true;
      }else{
        return false;  
      }
      
    }
    public function getUserByEmail($email){
            $stmt = $this->con->prepare("SELECT id, email, password, nama FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute(); 
            $stmt->bind_result($id, $email, $password, $nama);
            $stmt->fetch(); 
            $user = array(); 
            $user['id'] = $id; 
            $user['email']=$email; 
            $user['password'] = $password; 
            $user['nama'] = $nama; 
            return $user; 
    }

    private function getUsersPasswordByEmail($email){
            $stmt = $this->con->prepare("SELECT password FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute(); 
            $stmt->bind_result($password);
            $stmt->fetch(); 
            return $password; 
        }

    public function isEmailExist($email){
      $stmt = $this->con->prepare("SELECT id FROM users WHERE email = ?");
      $stmt->bind_param("s",$email);
      $stmt->execute();
      $stmt->store_result();
      return $stmt->num_rows > 0;
    }

    public function userLogin($email,$password){
      if($this->isEmailExist($email)){
        $hashed_password = $this->getUsersPasswordByEmail($email); 
        if(password_verify($password, $hashed_password)){
          return USER_AUTHENTICATED;
        }else{
          return USER_PASSWORD_DO_NOT_MATCH; 
        }
      }
      else{
        return USER_NOT_FOUND; 
      }
    }
  }
?>
